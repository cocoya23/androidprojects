package eja.org.demopreferenceshelper.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import eja.org.demopreferenceshelper.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by cocoy on 30/01/2016.
 */

@ContentView(R.layout.main_layout)
public class MainAcitity extends BaseActionBarActivity implements View.OnClickListener{

    @InjectView(R.id.test_action)
    private Button testAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        testAction.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.preferencesHelper.saveUsername("enrique");
        Toast.makeText(this,super.preferencesHelper.getUsername(),Toast.LENGTH_LONG).show();
    }
}
