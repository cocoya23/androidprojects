package eja.org.demopreferenceshelper.core.modules;

import com.google.inject.AbstractModule;

import eja.org.demopreferenceshelper.core.commons.helpers.PreferencesHelper;

/**
 * Created by cocoy on 30/01/2016.
 */
public class HelpersModule extends AbstractModule {


    @Override
    protected void configure() {
        bind(PreferencesHelper.class).asEagerSingleton();
    }
}
