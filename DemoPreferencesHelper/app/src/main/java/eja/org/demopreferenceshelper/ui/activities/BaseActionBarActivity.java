package eja.org.demopreferenceshelper.ui.activities;

import android.os.Bundle;

import com.google.inject.Inject;

import eja.org.demopreferenceshelper.core.commons.helpers.PreferencesHelper;
import roboguice.activity.RoboActionBarActivity;

/**
 * Created by cocoy on 30/01/2016.
 */
public class BaseActionBarActivity extends RoboActionBarActivity {

    @Inject
    protected PreferencesHelper preferencesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
