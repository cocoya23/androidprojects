package eja.org.demopreferenceshelper.core.commons.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.inject.Inject;

public class PreferencesHelper {

    private final String PREFERENCES_TAG = "MyApp";
    private final String USERNAME_PREFERENCE = "username";
    private SharedPreferences preferences;

    @Inject
    public PreferencesHelper(Context context){
        preferences = context.getSharedPreferences(PREFERENCES_TAG,Context.MODE_PRIVATE);
    }

    public void saveUsername(String user){
        preferences.edit().putString(USERNAME_PREFERENCE,user).commit();
    }

    public String getUsername(){
        return preferences.getString(USERNAME_PREFERENCE,null);
    }

}
