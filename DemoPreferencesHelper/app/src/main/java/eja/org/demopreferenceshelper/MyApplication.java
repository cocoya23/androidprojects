package eja.org.demopreferenceshelper;

import android.app.Application;

import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.util.Modules;

import java.util.ArrayList;
import java.util.List;

import eja.org.demopreferenceshelper.core.modules.HelpersModule;
import roboguice.RoboGuice;

/**
 * Created by cocoy on 30/01/2016.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Injector injector = RoboGuice.getOrCreateBaseApplicationInjector(this,RoboGuice.DEFAULT_STAGE,getModules(this));
        injector.injectMembers(this);

    }

    private static Module[] getModules(final Application application){

        List<Module> modules = new ArrayList<>(0);

        modules.add(RoboGuice.newDefaultRoboModule(application));

        modules.add(new HelpersModule());

        return modules.toArray(new Module[modules.size()]);

    }

}
