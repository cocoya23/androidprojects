package org.eja.demopreferences.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.eja.demopreferences.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.Inflater;

/**
 * Created by cocoy on 23/08/2015.
 */
public class MainActivity extends ActionBarActivity {

    private boolean showToast;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.main_activity_layout);


    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        showToast = preferences.getBoolean(
                getString(R.string.show_toast_preference),true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.settings_item){

        }
        return true;
    }

    public void guardar(View view){

        SharedPreferences preferencias = getPreferences(0);
        SharedPreferences.Editor editor= preferencias.edit();

        editor.putString("mensaje","Se guardo un valor");
        editor.commit();

    }

    public void leer(View view){

        SharedPreferences preferencias = getPreferences(0);
        String mensaje = preferencias.getString("mansaje", "no encontre nada");

        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();

    }

    public void internalStorage(View view){

        File ruta = getFilesDir();

        /*getDir("pdf",MODE_PRIVATE);
        deleteFile("");
        fileList();*/

        try {
            FileOutputStream fos = openFileOutput("notas.txt",MODE_PRIVATE);
            fos.write("Hola mundo, k hace".getBytes());
            fos.flush();
            fos.close();

            FileInputStream fis = openFileInput("notas.txt");
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);

            String contenido = new String(bytes);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
