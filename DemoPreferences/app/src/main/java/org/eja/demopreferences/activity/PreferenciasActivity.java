package org.eja.demopreferences.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import org.eja.demopreferences.R;

/**
 * Created by cocoy on 23/08/2015.
 */
public class PreferenciasActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
