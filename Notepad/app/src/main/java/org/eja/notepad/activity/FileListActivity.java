package org.eja.notepad.activity;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.eja.notepad.R;

import java.io.File;

public class FileListActivity extends AppCompatActivity
implements AdapterView.OnItemClickListener{

    private ListView filesList;
    private ArrayAdapter<String> filesAdapter;
    public static final String FILE_NAME_EXTRA = "FILE_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);

        filesList = (ListView) findViewById(R.id.list_files);
        filesList.setOnItemClickListener(this);

        readFiles();

    }

    @Override
    protected void onResume() {
        super.onResume();
        readFiles();



    }

    private void readFiles(){

        File directorio = getFilesDir();

        filesAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                directorio.list()
        );

        filesList.setAdapter(filesAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_file_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_new) {
            Intent intento = new Intent(this,NoteActivity.class);
            startActivity(intento);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intento = new Intent(this,NoteActivity.class);
        intento.putExtra(FILE_NAME_EXTRA,filesAdapter.getItem(position));
        startActivity(intento);
    }
}
