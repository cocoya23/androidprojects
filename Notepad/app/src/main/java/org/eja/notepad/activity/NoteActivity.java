package org.eja.notepad.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.eja.notepad.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class NoteActivity extends AppCompatActivity {

    private EditText fileNameTxt,fileContentTxt;
    private String fileName;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        fileNameTxt = (EditText)findViewById(R.id.file_name);
        fileContentTxt = (EditText)findViewById(R.id.file_content);

        if((fileName = getIntent().getStringExtra(FileListActivity.FILE_NAME_EXTRA))!=null){
            isEdit = true;

            File archivo = new File(getFilesDir(),fileName);

            fileNameTxt.setText(archivo.getName());
            try {
                FileReader fileReader = new FileReader(archivo);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String s;
                StringBuilder builder = new StringBuilder();
                while((s = bufferedReader.readLine()) != null) {
                    builder.append(s).append("\n");
                }
                bufferedReader.close();
                fileReader.close();

                fileContentTxt.setText(builder.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(isExternalStorageReadable()){
            File externalStorage = getExternalFilesDir(null);

            Toast.makeText(
                    this,
                    externalStorage.getAbsolutePath(),
                    Toast.LENGTH_LONG
            ).show();
        }

        if(isExternalStorageReadable()){
            File externalStorage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            //Environment.DIRECTORY_DCIM;

            File archivo = new File(externalStorage,"carpeta");
            if(archivo.mkdir()){
                System.out.println("se creo carpeta");
            }
            /*try {
                FileWriter fileWriter = new FileWriter(archivo);
                fileWriter.write("algo");
                fileWriter.flush();
                fileWriter.close();

                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            System.out.println(externalStorage.canRead());
            StringBuilder builder = new StringBuilder();
            for(File file:externalStorage.listFiles()){
                builder.append(file.getName()).append("-");
            }
            System.out.println(builder.toString());
            Toast.makeText(
                    this,
                    externalStorage.getAbsolutePath(),
                    Toast.LENGTH_LONG
            ).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note, menu);
        if(!isEdit)
            menu.findItem(R.id.action_delete).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            String fileName = fileNameTxt.getText().toString();
            if( isEdit && !this.fileName.equals(fileName)){
                deleteFile(this.fileName);
            }
            File archivo = new File(getFilesDir(),fileName);
            try {
                FileWriter fileWriter = new FileWriter(archivo);
                fileWriter.write(fileContentTxt.getText().toString());
                fileWriter.flush();
                fileWriter.close();

                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else if (id == R.id.action_delete) {
            deleteFile(this.fileName);
            finish();
        } else if (id == R.id.action_share) {
            /*Uri uri = Uri.parse("tel:58843700");
            Intent intento = new Intent(Intent.ACTION_DIAL,uri);*/
            /*Uri uri = Uri.parse("geo:0,0?q=30+Nevado+de+toluca");
            Intent intento = new Intent(Intent.ACTION_VIEW,uri);*/

            Intent intento = new Intent(Intent.ACTION_SEND);
            intento.setType("text/plain");
            intento.putExtra(Intent.EXTRA_SUBJECT, fileNameTxt.getText().toString());
            intento.putExtra(Intent.EXTRA_TEXT,fileContentTxt.getText().toString());

            Intent chooser = Intent.createChooser(intento,"Elige como compartir");

            if(intento.resolveActivity(getPackageManager())!=null){
                startActivity(chooser);
            } else {
                Toast.makeText(this,"no hay",Toast.LENGTH_LONG).show();
            }

            /*PackageManager packageManager = getPackageManager();
            List<ResolveInfo> lista = packageManager.queryIntentContentProviders(intento,PackageManager.MATCH_ALL);

            if(lista.size()>0){
                startActivity(intento);
            } else {
                Toast.makeText(this,"no hay",Toast.LENGTH_LONG).show();
            }*/


        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isExternalStorageWriteable(){
        String state = Environment.getExternalStorageState();
        if(state.equals(Environment.MEDIA_MOUNTED))
            return true;

        return false;
    }

    public boolean isExternalStorageReadable(){
        String state = Environment.getExternalStorageState();
        if(state.equals(Environment.MEDIA_MOUNTED) ||
                state.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
            return true;
        return false;
    }
}
